﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Josephus
{
    public class JosephusSurvivor
    {
        public static int JosSurvivor(int anzahl, int schrittweite)
        {
            // Die Liste befüllen 
            //List<int> list = Enumerable.Range(1, n).ToList();
            List<int> list = new List<int>();
            
            for (int i = 1; i <= anzahl; i++)
            {
                list.Add(i);
            }

            // den Index initialisieren. Wir starten beim Index 0
            int index = 0;
            
            // Solange noch Elemente in der Liste sind, sollen wir welche entfernen
            while (list.Count > 1)
            {
                // Wir verschieben den Index um die Schrittweite -1, weil das Element
                // an der ersten Stelle ja mitzählt.
                index += schrittweite-1;

                // Wenn der Index größer als die Länge, dann muss die Länge der Liste solange
                // abgezogen werden, bis der Index kleiner ist
                while(list.Count - 1 < index)
                {
                    index = index - list.Count;
                }
                
                // an der Stelle des Index können wir jetzt das Element entfernen
                list.RemoveAt(index);
            }

            // Da wir jetzt nur noch 1 Element haben, können wir einfach dieses Element
            //zurück geben.
            
            //return list.ElementAt(0);
            return list[0];
        }
    }
}
